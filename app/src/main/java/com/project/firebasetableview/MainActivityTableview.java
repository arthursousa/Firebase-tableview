package com.project.firebasetableview;

import android.app.AlertDialog;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivityTableview extends ActionBarActivity {

    private static final int REQUEST_CAMERA = 0;
    private static final int SELECT_FILE = 1;
    private TableLayout tableview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_main_activity_tableview);

        new ImgLoader().execute();
    }

    private class ImgLoader extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            final Firebase firebaseRef = new Firebase("https://glaring-heat-2746.firebaseio.com/");
            tableview = (TableLayout) findViewById(R.id.tableview);

            firebaseRef.child("pictures").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot snapshot, String previousChildKey) {

                    TableRow tableRow = new TableRow(MainActivityTableview.this);
                    tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));

                    //decoding base64 string into bmp and adding table row to tableview
                    byte[] decodedString = Base64.decode(snapshot.getValue().toString(), Base64.DEFAULT);
                    Bitmap decodedBmp = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                    ImageView imgView = new ImageView(MainActivityTableview.this);
                    imgView.setImageBitmap(Bitmap.createScaledBitmap(decodedBmp, 100, 100, false));

                    TextView textView = new TextView(MainActivityTableview.this);
                    textView.setText(snapshot.getKey());

                    tableRow.addView(imgView);
                    tableRow.addView(textView);
                    tableview.addView(tableRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    System.out.println("The" + dataSnapshot.getKey() + "was updated");
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    System.out.println("The" + dataSnapshot.getKey() + "was removed");
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    System.out.println("The" + dataSnapshot.getKey() + "was moved");
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    System.out.println("Error" + firebaseError.toString());
                }
            });

            return null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();

        inflater.inflate(R.menu.app_bar_layout, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_add_picture) {
            selectImage();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivityTableview.this);
        builder.setTitle("Upload picture");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Firebase firebaseRef = new Firebase("https://glaring-heat-2746.firebaseio.com/");
        Firebase postRef = firebaseRef.child("pictures");

        if (resultCode == RESULT_OK) {
            //sending img byte arrays in base64 to firebase database
            if (requestCode == REQUEST_CAMERA) {

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                Bitmap bmp = (Bitmap) data.getExtras().get("data");

                if(bmp != null) {
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);

                    byte[] imgByteArray = stream.toByteArray();
                    String imageFile = Base64.encodeToString(imgByteArray, Base64.DEFAULT);
                    postRef.push().setValue(imageFile);

                    //adding picture also to gallery
                    try {
                        String fileName = System.currentTimeMillis() + ".jpg";

                        File destination = new File(Environment.getExternalStorageDirectory(),
                                fileName);

                        FileOutputStream fileOutputStream = new FileOutputStream(destination);
                        fileOutputStream.write(imgByteArray);
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null,
                        null);
                Cursor cursor = cursorLoader.loadInBackground();
                cursor.moveToFirst();

                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                String selectedImagePath = cursor.getString(column_index);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);

                final int REQUIRED_SIZE = 200;
                int scale = 1;

                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;

                Bitmap bmp = BitmapFactory.decodeFile(selectedImagePath, options);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] imgByteArray = stream.toByteArray();
                String imageFile = Base64.encodeToString(imgByteArray, Base64.DEFAULT);

                postRef.push().setValue(imageFile);
            }
        }
    }
}